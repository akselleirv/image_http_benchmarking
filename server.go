package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/akselsle/image_http_benchmarking/json_base64/server_JSON"
	"gitlab.com/akselsle/image_http_benchmarking/multiform-data/server_multi"
	"gitlab.com/akselsle/image_http_benchmarking/protobuf/server_proto"
	"io"
	"os"
)

func Logger() gin.HandlerFunc {
	return gin.LoggerWithFormatter(func(param gin.LogFormatterParams) string {
		// custom format
		return fmt.Sprintf("%s - [%d] - %s \n",
			param.Latency,
			param.StatusCode,
			param.Path,
		)
	})
}

func setupLogOutput() {
	f, _ := os.Create("benchmark_multi.log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}

func main() {
	setupLogOutput()
	r := gin.New()

	r.Use(Logger(),gin.Recovery())

	r.GET("/", func(context *gin.Context) {
		context.JSON(200, gin.H{
			"message": "it works",
		})
	})

	r.POST("/json", server_JSON.HandleJSONImage)
	r.POST("/multi",server_multi.HandleMultiImage)
	r.POST("/proto",server_proto.HandleProtoImage)


	r.Run()

}
