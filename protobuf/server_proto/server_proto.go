package server_proto

import (
	"github.com/gin-gonic/gin"
	"github.com/golang/protobuf/proto"
	proto_comm "gitlab.com/akselsle/image_http_benchmarking/protobuf"
	"net/http"
	"fmt"
)

func HandleProtoImage(ctx *gin.Context){
	image := new(proto_comm.Image)
	raw,err := ctx.GetRawData()
	if err != nil {
		ctx.JSON(http.StatusBadRequest,gin.H{"error":err.Error()})
		return
	}
	err = proto.Unmarshal(raw,image)
	if err != nil {
		ctx.JSON(http.StatusBadRequest,gin.H{"error":err.Error()})
		return
	}
	fmt.Println(len(image.Image))
}