package main

import (
	"bytes"
	"github.com/golang/protobuf/proto"
	proto_comm "gitlab.com/akselsle/image_http_benchmarking/protobuf"
	"io/ioutil"
	"net/http"
	"os"
	"fmt"
)

func main() {
	//Creating the proto struct
	imageProto := &proto_comm.Image{}

	file, err := os.Open("../../image.png")
	if err != nil {
		fmt.Println("image not found", err)
		return
	}
	defer file.Close()
	image, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
		return
	}
	imageProto.Image = image

	//encoding the image
	data, err := proto.Marshal(imageProto)
	if err != nil {
		fmt.Println(err)
		return
	}
	r := bytes.NewReader(data)
	res, err := http.Post("http://127.0.0.1:8080/proto", "application/protobuf", r)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(res.StatusCode)
}
