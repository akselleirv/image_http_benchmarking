package server_multi

import (
	"fmt"
	"github.com/gin-gonic/gin"

	"io/ioutil"
	"net/http"
)

func HandleMultiImage(ctx *gin.Context) {
	file, _, err := ctx.Request.FormFile("image")
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		fmt.Println(err)
		return
	}
	defer file.Close()
	b, err := ioutil.ReadAll(file)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		fmt.Println(b) //only to use b, error otherwise.
	}

	fmt.Println(len(b))
}
