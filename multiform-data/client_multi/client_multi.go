package main

import (
	"bytes"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"os"
)

func createRequest() *http.Request {
	//Creating buffer
	requestBody := &bytes.Buffer{}
	//Creating multipart form writer
	multipartWriter := multipart.NewWriter(requestBody)
	//************* Adding reference_image to form field **********************
	imagePathReference := "../../image.png"
	//opening test image:
	image, err := os.Open(imagePathReference)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	defer image.Close()

	h := make(textproto.MIMEHeader)
	h.Set("Content-Disposition",
		fmt.Sprintf(`form-data; name="%s"; filename="%s"`,"image","image.png"))
	h.Set("Content-Type", "image/png")
	w,err := multipartWriter.CreatePart(h)

	if err != nil {
		return nil
	}
	if _, err = io.Copy(w, image); err != nil {
		fmt.Println(err)
		return nil
	}
	multipartWriter.Close()
	req,err := http.NewRequest("POST","http://127.0.0.1:8080/multi",requestBody)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	req.Header.Set("Content-Type", multipartWriter.FormDataContentType())

	return req
}

func main() {
	var client = new(http.Client)

	res,err := client.Do(createRequest())
	if err != nil {
		println(err)
		return
	}
	println(res.StatusCode)

}
