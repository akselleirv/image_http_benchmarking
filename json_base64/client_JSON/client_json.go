package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func main() {
	type imageStruct struct {
		Image []byte `json:"image"`
	}

	file, err := os.Open("../../image.png")
	if err != nil {
		fmt.Println("image not found", err)
		return
	}
	defer file.Close()
	image, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
		return
	}
	imageBase64 := make([]byte, base64.StdEncoding.EncodedLen(len(image)))
	base64.StdEncoding.Encode(imageBase64, image)
	imageEncoded := imageStruct{Image: imageBase64}
	imageJSON,err := json.Marshal(imageEncoded)
	if err != nil {
		fmt.Println(err)
		return
	}
	b := bytes.NewBuffer(imageJSON)
	resp, err := http.Post("http://127.0.0.1:8080/json","application/json",b)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(resp)

}
