package server_JSON

import (
	"encoding/base64"
	"github.com/gin-gonic/gin"
	"net/http"
)

func HandleJSONImage(ctx *gin.Context) {

	type Form struct {
		Image []byte `json:"image"`
	}
	var json Form
	if err := ctx.ShouldBindJSON(&json); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	//fmt.Println("image base64 is ", len(json.Image))
	image := make([]byte, base64.StdEncoding.DecodedLen(len(json.Image)))
	base64.StdEncoding.Decode(image, json.Image)
	//fmt.Println("image is ", len(image))

	ctx.JSON(http.StatusOK,gin.H{"message":"good"})


}
