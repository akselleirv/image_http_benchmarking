import matplotlib.pyplot as plt

#creating lists
proto = []
json = []
multi = []

#files
protoFile = "../benchmark_proto.log"
jsonFile = "../benchmark_json.log"
multiFile = "../benchmark_multi.log"
def average(lst):
    return sum(lst) / len(lst)

def parseLog(fileName, list_of_data):
    with open(fileName) as f:
        for line in f:  
            split = line.split("-")
            time = split[0]
            time = time[:-3]
            time = float(time)

            list_of_data.append(time)

#parsing files
parseLog(protoFile,proto)
parseLog(jsonFile,json)
parseLog(multiFile,multi)

#plotting data
plt.plot(proto)
plt.plot(json)
plt.plot(multi)

#adding labels
plt.ylabel('time - ms')
plt.xlabel('POST request #')

#averages
avg_p = round(average(proto),3)
avg_j = round(average(json),3)
avg_m = round(average(multi),3)

p = 'protobuff ( ' + str(avg_p) + 'ms)'
j = 'json(b64) ( ' + str(avg_j) + 'ms)'
m = 'multi ( ' + str(avg_m) + 'ms)'

plt.legend([p,j,m],loc='best')

plt.show()
